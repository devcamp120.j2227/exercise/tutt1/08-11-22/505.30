// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");

// Khởi tạo app express
const app = express();

// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

// Khai báo để app đọc được request body json
app.use(express.json());

// Khai báo API
app.get("/", (req, res) => {
    let today = new Date();

    // Response trả 1 chuỗi JSON có status 200
    res.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

// Request params
// Được đặt sẵn vào URL
// Request param thường được sử dụng với những phương thức GET, PUT, DELETE
// VD: https://docs.google.com/presentation/d/108ppjKp_ac0ZHNfrNdOyxUPAG9E7Ehf_nKOFs-NniU8/edit
app.get("/request-params/:param1/:param2/param", (req, res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.status(200).json({
        params: {
            param1,
            param2
        }
    })
})

// Request query
// Request query thường chỉ được sử dụng cho phương thức GET
// Do không kiểm soát được request query, nên cần bắt buộc validate request query
app.get("/request-query", (req, res) => {
    let query = req.query;

    res.status(200).json({
        query
    })
})

// Request body json
// Request body json thường được sử dụng trong phương thức POST, PUT
// Do không kiểm soát được request body json, nên cần bắt buộc validate request body json
// Cần khai báo ở dòng 11 để đọc được body json
app.post("/request-body-json", (req, res) => {
    let body = req.body;

    res.status(200).json({
        body
    }) 
})

// Callback function
// + Nó là 1 function
// + Nó là 1 tham số của 1 hàm khác
// + Nó sẽ chạy sau khi hàm chủ thể được thực hiện
// Lắng nghe app trên cổng được khai báo 8000
app.listen(port, () => {
    console.log("App listening on port: ", port);
});